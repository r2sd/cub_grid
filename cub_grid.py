from getpass import getpass
from pprint import pprint
from PyPDF2 import PdfFileReader
import json

def break_strings_by_size(string, size):
    '''
    input:
        string = hello
        size = 2

    output:
        ['he', 'll', 'o']
    '''
    if size == 0:
        return [string]
    lst = []
    for i in range(len(string) // size):
        lst.append(string[i * size: ((i * size) + 2)])

    reminder = len(string) % size
    if reminder > 0:
        lst.append(string[-reminder:])

    return lst

def handle_grid_pdf():
    file_name = input('Enter the file name (leave empty for "grid.pdf"):')
    if not file_name:
        file_name = 'grid'
    if not file_name.endswith('.pdf'):
        file_name += '.pdf'
    with open(file_name, 'rb') as file_object:
        input1 = PdfFileReader(file_object)
        password = getpass()
        input1.decrypt(password)
        code_string = input1.getPage(0).extractText().split(' ')[-1]
        code_list = break_strings_by_size(code_string,  2)
        dikt = {}
        # Sample code list: ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', '00', '00', '00', '00', '00', '00', '00', '00', 'A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2', '00', '00', '00', '00', '00', '00', '00', '00']
        for i in range(8):
            '''
            Maps indices 0 to 8, 1 to 9 and so on.
            '''
            # dikt['A1'] = '00'
            dikt[code_list[i]] = code_list[i + 8]  # Why add 8? Look at the sample code list.
        
        for i in range(16, 24):
            dikt[code_list[i]] = code_list[i + 8]
        
        return dikt

if __name__ == '__main__':
    dikt = handle_grid_pdf()
    while True:
        try:
            user_input = input('Enter the input: ')
            keys = break_strings_by_size(user_input.upper(), 2)
            strings = []

            print()

            for key in keys:
                print('{} {}'.format(key, dikt[key]))
                strings.append(dikt[key])

            print()
            print(' '.join(map(str, strings)))
            print()
        except Exception as e:
            print(e)
        finally:
            user_input = input('To try again, enter 1: ')
            if user_input != '1':
                break
