import unittest
from cub_grid import *

class CubGridTest(unittest.TestCase):
    def test_break_strings_by_size(self):
        self.assertListEqual(break_strings_by_size('hello', 2), ['he', 'll', 'o'])
        self.assertListEqual(break_strings_by_size('hell', 2), ['he', 'll'])
        self.assertListEqual(break_strings_by_size('', 2), [])
        self.assertListEqual(break_strings_by_size('hello', 0), ['hello'])
        self.assertListEqual(break_strings_by_size('hello', 7), ['hello'])

    def test_handle_grid_pdf(self):
        dikt = handle_grid_pdf()
        print('Enter the correct values for grid to test')
        num = int(input('Enter number of tests:'))
        for i in range(num):
            key = input('Enter key: ').upper()
            value = input('Enter value: ')
            self.assertEqual(dikt[key], value)
